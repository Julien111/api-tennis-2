<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220215231609 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chelem_tournois DROP FOREIGN KEY FK_C5B9BE4512EE547B');
        $this->addSql('DROP INDEX UNIQ_C5B9BE4512EE547B ON chelem_tournois');
        $this->addSql('ALTER TABLE chelem_tournois DROP joueur_vainqueur_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chelem_tournois ADD joueur_vainqueur_id INT NOT NULL');
        $this->addSql('ALTER TABLE chelem_tournois ADD CONSTRAINT FK_C5B9BE4512EE547B FOREIGN KEY (joueur_vainqueur_id) REFERENCES joueurs_tennis (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C5B9BE4512EE547B ON chelem_tournois (joueur_vainqueur_id)');
    }
}
