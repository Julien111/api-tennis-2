<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220215231825 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE joueurs_tennis ADD tournois_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE joueurs_tennis ADD CONSTRAINT FK_272D64BA752534C FOREIGN KEY (tournois_id) REFERENCES chelem_tournois (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_272D64BA752534C ON joueurs_tennis (tournois_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE joueurs_tennis DROP FOREIGN KEY FK_272D64BA752534C');
        $this->addSql('DROP INDEX UNIQ_272D64BA752534C ON joueurs_tennis');
        $this->addSql('ALTER TABLE joueurs_tennis DROP tournois_id');
    }
}
