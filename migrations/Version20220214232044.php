<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220214232044 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE chelem_tournois (id INT AUTO_INCREMENT NOT NULL, joueur_vainqueur_id INT NOT NULL, vainqueur TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_C5B9BE4512EE547B (joueur_vainqueur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chelem_tournois ADD CONSTRAINT FK_C5B9BE4512EE547B FOREIGN KEY (joueur_vainqueur_id) REFERENCES joueurs_tennis (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE chelem_tournois');
    }
}
