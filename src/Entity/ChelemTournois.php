<?php

namespace App\Entity;

use App\Repository\ChelemTournoisRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChelemTournoisRepository::class)]
#[ApiResource]
class ChelemTournois
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read:tournoi'])]
    private $id;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read:tournoi', 'write:JoueursTennis'])]
    private $vainqueur;

    #[ORM\OneToOne(mappedBy: 'tournois', targetEntity: JoueursTennis::class, cascade: ['persist', 'remove'])]   
    private $joueursTennis;
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVainqueur(): ?bool
    {
        return $this->vainqueur;
    }

    public function setVainqueur(bool $vainqueur): self
    {
        $this->vainqueur = $vainqueur;

        return $this;
    }

    public function getJoueursTennis(): ?JoueursTennis
    {
        return $this->joueursTennis;
    }

    public function setJoueursTennis(?JoueursTennis $joueursTennis): self
    {
        // unset the owning side of the relation if necessary
        if ($joueursTennis === null && $this->joueursTennis !== null) {
            $this->joueursTennis->setTournois(null);
        }

        // set the owning side of the relation if necessary
        if ($joueursTennis !== null && $joueursTennis->getTournois() !== $this) {
            $joueursTennis->setTournois($this);
        }

        $this->joueursTennis = $joueursTennis;

        return $this;
    }
    
}