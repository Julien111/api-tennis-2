<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\JoueursTennisRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;

#[ORM\Entity(repositoryClass: JoueursTennisRepository::class)]
#[ApiResource(
    normalizationContext: [
        'groups' => ['read:collection'],
    ],
    denormalizationContext: [
        'groups' => ['write:JoueursTennis'],
    ],
    paginationItemsPerPage: 3,
    paginationMaximumItemsPerPage: 3,
    paginationClientItemsPerPage: true,
    collectionOperations: [
        'get',
        'post' => ['validation_groups' => ['create:post']]
    ],    
    itemOperations: ['put', 'delete', 'get' => ['normalization_context' => ['groups' => ['read:collection', 'read:item', 'read:tournoi']]],   
    ],        
)]
class JoueursTennis
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read:collection'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:collection', 'write:JoueursTennis']), Length(min: 2, groups: ['create:post'])]    
    private $nom;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:collection', 'write:JoueursTennis']), Length(min: 2, groups: ['create:post'])]
    private $prenom;

    #[ORM\Column(type: 'string', length: 5)]
    #[Groups(['read:collection', 'write:JoueursTennis'])]
    private $sexe;

    #[ORM\Column(type: 'text')]
    #[Groups(['read:item', 'write:JoueursTennis'])]
    private $description;

    #[ORM\OneToOne(inversedBy: 'joueursTennis', targetEntity: ChelemTournois::class, cascade: ['persist'])]    
    #[Groups(['read:item', 'write:JoueursTennis'])]
    private $tournois;
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTournois(): ?ChelemTournois
    {
        return $this->tournois;
    }

    public function setTournois(?ChelemTournois $tournois): self
    {
        $this->tournois = $tournois;

        return $this;
    }
    
}