<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class JoueursTennisController extends AbstractController
{
    #[Route('/joueurs/tennis', name: 'joueurs_tennis')]
    public function index(): Response
    {
        return $this->render('joueurs_tennis/index.html.twig', [
            'controller_name' => 'JoueursTennisController',
        ]);
    }
}
